#include "Direction.h"
#include "CarLight.h"
#include "PedestrianLight.h"
#include "Timer.h"
#include <LiquidCrystal_I2C.h>

// LDR Characteristics
const float GAMMA = 0.7;
const float RL10 = 50;

// PINS
int photo = 17;
// WE
int carWE_g = 7;
int carWE_y = 6;
int carWE_r = 5;
// SN
int carSN_r = 4;
int carSN_y = 3;
int carSN_g = 2;
// WE
int pedWE_g = 12;
int pedWE_r = 11;
int pedWE_b = 13;
// SN
int pedSN_g = 8;
int pedSN_r = 9;
int pedSN_b = 10;

int buttons[] = {pedWE_b, pedSN_b}; 
int LEDs[] = {carWE_g, carWE_y, carWE_r, carSN_r, carSN_y, carSN_g, pedWE_g, pedWE_r, pedSN_g, pedSN_r };

int basic_time_to_go = 12000;
int basic_time_to_switch = 3000;
int time_to_blink = 2000;

class Crossing 
{
  CarLight carLightWE = CarLight(carWE_r, carWE_y, carWE_g, WE);
  CarLight carLightSN = CarLight(carSN_r, carSN_y, carSN_g, SN);
  PedestrianLight pedLightWE = PedestrianLight(pedWE_r, pedWE_g, pedWE_b, WE);
  PedestrianLight pedLightSN = PedestrianLight(pedSN_r, pedSN_g, pedSN_b, SN);
  Timer timer;
  Direction currentDirection = WE;
  public:
    void Reset()
    {
      carLightWE.Reset();
      carLightSN.Reset();
      pedLightWE.Reset();
      pedLightSN.Reset();
    }
    void DayLoop()
    {
      //Go in one direction  
      Serial.println("Traffic is open in " + DirectionToString(WE) + " direction");
      carLightWE.Go();
      pedLightWE.Go();
      pedLightSN.Stop();
      carLightSN.Stop();
      timer.Start(basic_time_to_go);  
      while(timer.TimeLeft() != 0 )
      {    
        if (pedLightSN.IsButtonPushed())
          timer.AdjustTime();    
      }

      //Switch
      Serial.println("Switching...");
      carLightWE.PrepareToStop();
      pedLightWE.Stop();
      carLightSN.PrepareToGo();
      pedLightSN.Stop();
      delay(basic_time_to_switch);

      //Go in another direction
      Serial.println("Traffic is open in " + DirectionToString(SN) + " direction");
      carLightSN.Go();
      pedLightSN.Go();
      carLightWE.Stop();  
      pedLightWE.Stop();  
      timer.Start(basic_time_to_go);  
      while(timer.TimeLeft() != 0 )
      {  
        if (pedLightWE.IsButtonPushed())
          timer.AdjustTime();    
      }

      //Switch
      Serial.println("Switching...");
      carLightSN.PrepareToStop();
      pedLightSN.Stop();
      carLightWE.PrepareToGo(); 
      pedLightWE.Stop();  
      delay(basic_time_to_switch);
    }
    void NightLoop()
    {
      pedLightSN.Reset();
      pedLightWE.Reset();
      
      carLightSN.PrepareToStop();
      carLightWE.PrepareToStop();
      delay(time_to_blink);
      carLightSN.Reset();
      carLightWE.Reset();
      delay(time_to_blink);      
    }
};

LiquidCrystal_I2C lcd(0x27, 20, 4);
Crossing crossing;

void setup() {
  Serial.begin(115200);
  int size = sizeof(LEDs) / sizeof(int);
  // put your setup code here, to run once:
  for (int i = 0; i < size; ++i)
  {
    pinMode(LEDs[i], OUTPUT);
  }
  size = sizeof(buttons) / sizeof(int);
  for (int i = 0; i < size; ++i)
  {
    pinMode(buttons[i], INPUT_PULLUP);
  }

  pinMode(photo, INPUT);
  lcd.init();
  lcd.backlight();
  crossing.Reset();
}

void loop() {
  // put your main code here, to run repeatedly:
  Serial.println("Start sequence");
  int analogValue = analogRead(A3);
  float voltage = analogValue / 1024. * 5;
  float resistance = 2000 * voltage / (1 - voltage / 5);
  float lux = pow(RL10 * 1e3 * pow(10, GAMMA) / resistance, (1 / GAMMA));
  lcd.setCursor(2, 0);
  if (lux > 50) {
    lcd.print("Day, lux: ");   
    lcd.print(lux);   
    crossing.DayLoop();     
  } 
  else {    
    lcd.print("Night, lux: ");
    lcd.print(lux);   
    crossing.NightLoop();
  }
  Serial.println("End sequence\n");
}