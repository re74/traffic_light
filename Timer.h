class Timer
{
  long time = -1;
  long started = -1;
  public:
    void Start(int t) {
      this->time = t;
      this->started = millis();
    }
    int ElapsedTime() {
      if (millis()-started >= time)
        return -1;
      else 
        return millis()-started;
    }
    int TimeLeft() {
      long current = millis();
      if (current-started >= time)
      {
        return 0;
      }
      else 
        return time - (current - started); 
    }
    void AdjustTime() {
      Serial.println("Adjusting time...");
      float elapsed_to_time = ElapsedTime()/time;
      if (elapsed_to_time < 0.5)
      {
        time *= 0.6;
      }        
      else if (elapsed_to_time < 0.75)
      {
        time *= 0.85;
      }
    }
};