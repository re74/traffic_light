# Traffic Light simulation

This project is an assignment in C++/Embedded course. This is a traffic lights simulation. <br>
The assignment is done using online simulator found [here](https://wokwi.com/). <br> 


## Usage

* Go to [Wokwi.org](https://wokwi.com/)
* Scroll down to "Start from scratch" section. 
* Choose Arduino Nano.
* Copy the contents of files from the repository to the corresponding tabs on the left side of the window.
* Press play-button to see the simulation in action.

The traffic lights for cars are the groups of 3 lights.<br> 
The traffic lights for pedestrians are the groups of 2 lights: <br>
![scheme](scheme.png)

During simulation the console will output corresponding messages, for example: 
```sh
<--->
Start sequence
Traffic is open in West-East direction
Pedestrians in the South-North direction pushed the button
Adjusting time...
Switching...
Traffic is open in South-North direction
Switching...
End sequence

Start sequence
Traffic is open in West-East direction
Switching...
Traffic is open in South-North direction
Switching...
End sequence

Start sequence
Traffic is open in West-East direction
<--->
```
### Maintainers
@MariaNema
