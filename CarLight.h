class CarLight{
  private:
    int red;
    int yellow;
    int green;
    Direction direction;
  public:
    CarLight(int r, int y, int g, Direction d ): red(r), green(g), yellow(y), direction(d){}
    void Reset() {
      Serial.println("Resetting " + DirectionToString(direction) + " car traffic light");
      digitalWrite(green, LOW);
      digitalWrite(red, LOW);
      digitalWrite(yellow, LOW);
    }
    void Go() {
      //Serial.println("Cars CAN GO in the " + DirectionToString(direction) + " direction");
      digitalWrite(green, HIGH);
      digitalWrite(yellow, LOW);
      digitalWrite(red, LOW);
    }
    void Stop() {
      //Serial.println("Cars CAN NOT GO in the " + DirectionToString(direction) + " direction");
      digitalWrite(red, HIGH);
      digitalWrite(yellow, LOW);
      digitalWrite(green, LOW);
    }
    void PrepareToGo() {
      //Serial.println("Cars CAN SOON GO in the " + DirectionToString(direction) + " direction");
      digitalWrite(red, HIGH);
      digitalWrite(yellow, HIGH);
      digitalWrite(green, LOW);
    }
    void PrepareToStop() {
      //Serial.println("Cars in the " + DirectionToString(direction) + " direction WILL SOON HAVE TO STOP");
      digitalWrite(yellow, HIGH);
      digitalWrite(red, LOW);
      digitalWrite(green, LOW);
    }
};