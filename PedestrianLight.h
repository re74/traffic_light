class PedestrianLight {
  private:
    int red;
    int green;
    int button;
    int buttonBlocked = true;
    Direction direction;
  public:
    PedestrianLight(int r, int g, int b, Direction d ): red(r), green(g), button(b), direction(d) {}
    void Reset() {
      //Serial.println("Resetting " + DirectionToString(direction) + " pedestrian traffic light");
      digitalWrite(green, LOW);
      digitalWrite(red, LOW);
      buttonBlocked=true;
    }
    void Go() {
      //Serial.println("Pedestrians CAN GO in the " + DirectionToString(direction) + " direction");
      digitalWrite(green, HIGH);
      digitalWrite(red, LOW);
      buttonBlocked=true;
    }
    void Stop() {
      //Serial.println("Pedestrians CAN NOT GO in the " + DirectionToString(direction) + " direction");
      digitalWrite(red, HIGH);
      digitalWrite(green, LOW);
      buttonBlocked=false;
    }
    bool IsButtonPushed() {
      if (!buttonBlocked && digitalRead(button) == LOW)
      {
        Serial.println("Pedestrians in the " + DirectionToString(direction) + " direction pushed the button");
        buttonBlocked = true;
        return true;
      }
      return false;
    }
};