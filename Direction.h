enum Direction {WE, SN};
String DirectionToString(Direction dir)
{
  if (dir == WE)
    return "West-East";
  else  
    return "South-North";
}
